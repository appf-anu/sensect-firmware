// Select your modem:
// #define TINY_GSM_MODEM_UBLOX
#define TINY_GSM_MODEM_SARAR4
// #define TINY_GSM_MODEM_BG96

#define DEBUG

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG Serial

// Range to attempt to autobaud

#define TINY_GSM_USE_GPRS true

#include <TinyGsmClient.h>
#include <MQTT.h>
#ifdef DEBUG
#include <StreamDebugger.h>
StreamDebugger debugger(SerialSARA, Serial);
TinyGsm modem(debugger);
#elif
TinyGsm modem(SerialSARA);
#endif
TinyGsmClient client(modem);
MQTTClient mqtt;

#define LED_PIN 13
int ledStatus = LOW;

unsigned long lastMillis = 0;

void connect() {
  // connection state
  bool connected = false;


  Serial.print("\nconnecting...");
  while (!mqtt.connect("gsmclient", "mqtt", "ng4rtxlm5yt78bzz")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  mqtt.subscribe("gsmclient/+");
  // client.unsubscribe("/hello");
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);

  // Note: Do not use the client in the callback to publish, subscribe or
  // unsubscribe as it may cause deadlocks when other things arrive while
  // sending and receiving acknowledgments. Instead, change a global variable,
  // or push to a queue and handle it in the loop after calling `client.loop()`.
}

void setup()
{
    // Set console baud rate
    Serial.begin(9600);
    delay(10);

    pinMode(LED_PIN, OUTPUT);
    SerialSARA.begin(115200);

    Serial.println("Wait...");

    delay(6000);

    // Restart takes quite some time
    // To skip it, call init() instead of restart()
    Serial.println("Initializing modem...");
    modem.restart();
    // modem.init();

    
    Serial.print("Modem Info: ");
    Serial.println(modem.getModemInfo());

    Serial.print("Waiting for network...");
    while (!modem.waitForNetwork())
    {
        Serial.println("waiting");
        delay(10000);
    }
    Serial.println(" success");

    if (modem.isNetworkConnected())
    {
        Serial.println("Network connected");
    }

    if (!modem.isGprsConnected()) {
        modem.gprsConnect("");
    }else{
        Serial.println("gprs connected");
    }
    mqtt.begin(client);

    mqtt.setHost("mqtt.dunwell.xyz", 1883);
    connect();
}

void loop() {
  mqtt.loop();
  if (!modem.isGprsConnected()) {
      modem.gprsConnect("");
  }
  if (!mqtt.connected()) {
    connect();
  }
  // publish a message roughly every second.
  if (millis() - lastMillis > 1000) {
    lastMillis = millis();
    Serial.println("publishing...");
    mqtt.publish("hello", "world");
  }
}