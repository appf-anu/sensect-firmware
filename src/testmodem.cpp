// /*
//    DebugATCommandsSARA sketch

//    This sketch is modified from SerialSARAPassthrough.

//    The sketch allows you to send 15 basic AT commands from the USB CDC serial port
//    of the MKR NB 1500 board to the onboard ublox SARA-R410 celluar module.

//    If you want to add more commands the list of supported AT commands are found here:
//    https://www.u-blox.com/sites/default/files/u-blox-CEL_ATCommands_%28UBX-13002752%29.pdf

//    Circuit:
//    - MKR NB 1500 board
//    - Antenna
//    - 1500 mAh or higher lipo battery connected recommended
//        - USB seems to have enough power (tested with my PC)
//    - SIM card

//    Make sure the Serial Monitor's line ending is set to "Both NL and CR"

//    create 26 February 2019
//    Tomi Sarajisto
// */
// #include <Arduino.h>
// // baud rate used for both Serial ports
// unsigned long baud = 115200;
// char command;

// void setup()
// {

//     Serial.begin(baud);
//     SerialSARA.begin(baud);
// }

// void loop()
// {
//     if (Serial.available())
//     {
//         command = Serial.read();
//         switch (command)
//         {
//         case 'm':
//             Serial.println("m: Set verbose error resultcodes");
//             SerialSARA.println("AT+CMEE=2");
//             break;
//         case '1':
//             Serial.println("1: Check IMEI");
//             SerialSARA.println("AT+GSN");
//             break;
//         case '2':
//             Serial.println("2: Check IMSI");
//             SerialSARA.println("AT+CIMI");
//             break;
//         case '3':
//             Serial.println("3: Check RAT type config");
//             SerialSARA.println("AT+URAT?");
//             break;
//         case '4':
//             Serial.println("4: Check band configuration");
//             SerialSARA.println("AT+UBANDMASK?");
//             break;
//         case '5':
//             Serial.println("5: Check signal");
//             SerialSARA.println("AT+CSQ");
//             break;
//         case '6':
//             Serial.println("6: Check APN configuration");
//             SerialSARA.println("AT+CGDCONT?");
//             break;
//         case '7':
//             Serial.println("7: Check network");
//             SerialSARA.println("AT+COPS?");
//             break;
//         case '8':
//             Serial.println("8: Check all available networks (takes long time)");
//             SerialSARA.println("AT+COPS=?");
//             break;
//         case '9':
//             Serial.println("9: set cops=0 auto reg");
//             SerialSARA.println("AT+COPS=0");
//             break;
//         case 'q':
//             Serial.println("q: check cereg");
//             SerialSARA.println("AT+CEREG?");
//             break;
//         case 't':
//             Serial.println("t: set autoreg");
//             SerialSARA.println("AT+CEREG=1");
//             break;
//         case 'a':
//             Serial.println("a: Reset HTTP profile #0");
//             SerialSARA.println("AT+UHTTP=0");
//             break;
//         case 'b':
//             Serial.println("b: Set server domain name");
//             SerialSARA.println("AT+UHTTP=0,1,\"jsonplaceholder.typicode.com\"");
//             break;
//         case 'c':
//             Serial.println("c: Set to server port 80");
//             SerialSARA.println("AT+UHTTP=0,5,80");
//             break;
//         case 'd':
//             Serial.println("d: GET request");
//             SerialSARA.println("AT+UHTTPC=0,1,\"/posts/1\",\"get.ffs\"");
//             break;
//         case 'e':
//             Serial.println("e: Read response");
//             SerialSARA.println("AT+URDFILE=\"get.ffs\"");
//             break;
//         }
//     }

//     if (SerialSARA.available())
//     {
//         Serial.write(SerialSARA.read());
//     }
// }