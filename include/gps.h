#include <Arduino.h>
#include <Wire.h>                           //Needed for I2C to GPS
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS

SFE_UBLOX_GPS myGPS;

long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.

void setupGPS(){
  if (myGPS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1)
      ;
  }

  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.saveConfiguration();        //Save the current settings to flash and BBR
};

void printGPSData()
{
    if (millis() - lastTime > 1000)
    {
        lastTime = millis(); //Update the timer

        long latitude = myGPS.getLatitude();
        Serial.print(F("Lat: "));
        Serial.print(latitude);

        long longitude = myGPS.getLongitude();
        Serial.print(F(" Long: "));
        Serial.print(longitude);
        Serial.print(F(" (degrees * 10^-7)"));

        long altitude = myGPS.getAltitude();
        Serial.print(F(" Alt: "));
        Serial.print(altitude);
        Serial.print(F(" (mm)"));

        byte SIV = myGPS.getSIV();
        Serial.print(F(" SIV: "));
        Serial.print(SIV);

        Serial.println();
        Serial.print(myGPS.getYear());
        Serial.print("-");
        Serial.print(myGPS.getMonth());
        Serial.print("-");
        Serial.print(myGPS.getDay());
        Serial.print(" ");
        Serial.print(myGPS.getHour());
        Serial.print(":");
        Serial.print(myGPS.getMinute());
        Serial.print(":");
        Serial.print(myGPS.getSecond());

        Serial.print("  Time is ");
        if (myGPS.getTimeValid() == false)
        {
            Serial.print("not ");
        }
        Serial.print("valid  Date is ");
        if (myGPS.getDateValid() == false)
        {
            Serial.print("not ");
        }
        Serial.print("valid");

        Serial.println();
    }
};