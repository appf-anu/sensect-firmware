#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <SD.h>

// Our configuration structure.
//
// Never use a JsonDocument to store the configuration!
// A JsonDocument is *not* a permanent storage; it's only a temporary storage
// used during the serialization phase. See:
// https://arduinojson.org/v6/faq/why-must-i-create-a-separate-config-object/
struct Config
{
    char hostname[64];
    char username[64];
    char password[64];
    int port;
    bool tls;
};

const char *filename = "config.js"; // <- SD library uses 8.3 filenames

Config config; // <- global configuration object

// Loads the configuration from a file
bool loadConfiguration(const char *filename, Config &config)
{
    // Open file for reading
    File file = SD.open((char *)filename);

    // Allocate a temporary JsonDocument
    // Don't forget to change the capacity to match your requirements.
    // Use arduinojson.org/v6/assistant to compute the capacity.
    StaticJsonDocument<512> doc;

    // Deserialize the JSON document
    DeserializationError error = deserializeJson(doc, file);
    if (error)
        Serial.println(F("Failed to read file, using default configuration"));

    // Copy values from the JsonDocument to the Config

    strlcpy(config.hostname,                      // <- destination
            doc["hostname"] | "mqtt.example.com", // <- source
            sizeof(config.hostname));             // <- destination's capacity
    strlcpy(config.username,                      // <- destination
            doc["username"] | "user",             // <- source
            sizeof(config.username));             // <- destination's capacity
    strlcpy(config.password,                      // <- destination
            doc["password"] | "password",         // <- source
            sizeof(config.password));             // <- destination's capacity

    config.port = doc["port"] | 8883;
    config.tls = doc["tls"] | false;

    // Close the file (Curiously, File's destructor doesn't close the file)
    file.close();
    if (error)
        return false;
    return true;
};

// Saves the configuration to a file
void saveConfiguration(const char *filename, const Config &config)
{
    // Delete existing file, otherwise the configuration is appended to the file
    SD.remove((char *)filename);

    // Open file for writing
    File file = SD.open((char *)filename, FILE_WRITE);
    if (!file)
    {
        Serial.println(F("Failed to create file"));
        return;
    }

    // Allocate a temporary JsonDocument
    // Don't forget to change the capacity to match your requirements.
    // Use arduinojson.org/assistant to compute the capacity.
    StaticJsonDocument<256> doc;

    // Set the values in the document
    doc["hostname"] = config.hostname;
    doc["username"] = config.username;
    doc["password"] = config.password;
    doc["tls"] = config.tls;
    doc["port"] = config.port;

    // Serialize JSON to file
    if (serializeJson(doc, file) == 0)
    {
        Serial.println(F("Failed to write to file"));
    }

    // Close the file
    file.close();
};

// Prints the content of a file to the Serial
void printFile(const char *filename)
{
    // Open file for reading
    File file = SD.open((char *)filename);
    if (!file)
    {
        Serial.println(F("Failed to read file"));
        return;
    }

    // Extract each characters by one by one
    while (file.available())
    {
        Serial.print((char)file.read());
    }
    Serial.println();

    // Close the file
    file.close();
};

void setupConfig()
{
    // Initialize SD library (sensect chipselect is pin4)
    while (!SD.begin(4))
    {
        Serial.println(F("Failed to initialize SD library"));
        delay(1000);
    }

    // Should load default config if run for the first time
    Serial.println(F("Loading configuration..."));
    if (!loadConfiguration(filename, config))
    {
        // Create configuration file
        Serial.println(F("Saving default configuration..."));
        saveConfiguration(filename, config);
    }

    // Dump config file
    Serial.println(F("configuration: "));
    printFile(filename);
};
